package com.shopme.admin.user;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.Rollback;

import com.shopme.common.entity.Role;
import com.shopme.common.entity.User;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(false)
public class UserRepositoryTests {
    @Autowired
    private UserRepository repo;
    // Support data jpa test
    @Autowired
    private TestEntityManager entityManager;

    /**
     * Create new user
     */
    @Disabled
    public void testCreateUser() {
        // Get role from database by id.
        Role roleAdmin = entityManager.find(Role.class, 1);
        User userAdmin = new User("sss@fsot.com", "nam2020", "Nam", "Hoang Hai");

        userAdmin.addRole(roleAdmin);

        User savedUser = repo.save(userAdmin);

        assertThat(savedUser.getId()).isGreaterThan(0);

    }

    /**
     * Create user two roles
     */
    @Disabled
    public void testCreateNewUserTwoRoles() {
        User userRa = new User("Ra@fsot.com", "ra2020", "ra", "John");
        Role roleEditor = new Role(3);
        Role roleAssistant = new Role(5);

        userRa.addRole(roleEditor);
        userRa.addRole(roleAssistant);

        User savedUser = repo.save(userRa);

        assertThat(savedUser.getId()).isGreaterThan(0);

    }

    /**
     * Test get list user
     */
    @Disabled
    public void testListAllUsers() {
        Iterable<User> listUsers = repo.findAll();
        listUsers.forEach(user -> System.out.println(user));
    }

    /**
     * Test get user by id
     */
    @Disabled
    public void testGetUserById() {
        User userName = repo.findById(1).get();
        System.out.println(userName);
        assertThat(userName).isNotNull();
    }

    /**
     * Test Update
     */
    @Disabled
    public void testUpdateUserDetails() {
        User userName = repo.findById(2).get();
        userName.setEnabled(true);
        userName.setEmail("vila@gmail.com");

        repo.save(userName);
    }

    /**
     * Test Update
     */
    @Disabled
    public void testUpdateUserRoles() {
        User userRav = repo.findById(2).get();
        Role roleEditor = new Role(3);
        Role roleSalesperson = new Role(2);

        userRav.getRoles().remove(roleEditor);
        userRav.addRole(roleSalesperson);

        repo.save(userRav);
    }

    /**
     * Test Delete by Id
     */
    @Disabled
    public void testDeleteUser() {
        Integer userId = 2;
        repo.deleteById(userId);
    }

    @Test
    public void testGetUserByEmail() {
        String email = "Ra@fsoft.com";
        User user = repo.getUserByEmail(email);

        assertThat(user).isNotNull();
    }

}
